#remove obsolete programs
sudo apt-get update
sudo apt-get -y upgrade
#install software
sudo apt-get -y install build-essential git cmake libgtk2.0-dev pkg-config python-dev python-numpy libavcodec-dev libavcodec-extra libavformat-dev libswscale-dev chromium-browser mc vim ntp openshot octave texlive-full iceweasel unison kazam xclip curl exfat-utils exfat-fuse cifs-utils clang-3.5 parallel openssh-server gparted audacity inkscape texmaker gpsd-clients gpsd gpsbabel ranger gddrescue sshfs libboost-all-dev fxload gdb
sudo ln -s /usr/bin/clang-3.5 /usr/bin/clang
sudo ln -s /usr/bin/clang++-3.5 /usr/bin/clang++
# Time Servers
echo "...Setting Time Servers..."
sed -i '/server/d' /etc/ntp.conf
echo "#Durham uni time servers" >> /etc/ntp.conf
for num in `seq 1 4`;
do
        echo "server ntp$num.dur.ac.uk" >> /etc/ntp.conf
done
/etc/init.d/ntp restart
/etc/init.d/ntp status
ntpq -p
#create connection to vision-data, might require to put local.cifs file
sudo mkdir -p /mnt/vision-data
#install opencv
cd ~
mkdir opencv249
cd opencv249/
wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.9/opencv-2.4.9.zip
unzip opencv-2.4.9.zip
cd opencv-2.4.9/
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=Debug -D CMAKE_INSTALL_PREFIX=/usr -D CMAKE_CXX_FLAGS="-march=native" ..
make -j 8
sudo make install
#disable automatic updates
sudo sed -i '/APT::Periodic::Update-Package-Lists/d' /etc/apt/apt.conf.d/10periodic
echo "APT::Periodic::Update-Package-Lists \"0\";" | sudo tee -a /etc/apt/apt.conf.d/10periodic
echo "keymaps 0-127" > vim.kmap
echo "keycode 58 = Escape" >> vim.kmap
sudo cp vim.kmap /usr/local/etc/
echo "/usr/bin/loadkeys /usr/local/etc/vim.kmap" | sudo tee -a /etc/rc.local  
